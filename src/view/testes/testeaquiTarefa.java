package view.testes;

import model.dao.DaoFactory;
import model.dao.TarefaDao;
import model.entities.Curso;
import model.entities.Tarefa;
import model.enums.Dificuldade;

public class testeaquiTarefa {
	public static void main(String[] args) {
		
		TarefaDao tarefa = DaoFactory.createTarefaDao();
		Curso c = new Curso(4, null, null);
		Dificuldade d = Dificuldade.INICIANTE;
		
//		System.out.println("\n TESTE1 - INSERT TAREFA");
//		tarefa.insert(new Tarefa("Atividade1", c, null, d));
		
//		System.out.println("\n TESTE2 - DELETE TAREFA");
//		tarefa.deleteById("#9Inserida");
		
		
//		System.out.println("\n TESTE3 - UPDATE TAREFA");
//		tarefa.update(new Tarefa("#3", new Curso(3, null, null), null, d));
		
		
//		System.out.println("\n TESTE4 - FINDBYID TAREFA");
//		System.out.println(tarefa.findById("#3"));
		
//		System.out.println("\n TESTE4 - FINDBYCURSO TAREFA");
//		System.out.println(tarefa.findByCurso(c));
//		
		
//		System.out.println("\n TESTE5 - FINDALL TAREFA");
//		System.out.println(tarefa.findAll());
	}
}
